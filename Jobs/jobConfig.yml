# Jobs configuration.
#
# Stores information about each job.
#
# NOTE: When having multiple jobs, both jobs will give the income payout to the player
# even if they give the pay for one action (make the configurations with this in mind)
# and each job will get the respective experience.
#
# e.g If player has 2 jobs where job1 gives 10 income and experience for killing a player 
# and job2 gives 5 income and experience for killing a player. When the user kills a player
# they will get 15 income and job1 will gain 10 experience and job2 will gain 5 experience.

Jobs:
  # must be one word
  Woodcutter:
    # full name of the job (displayed when browsing a job, used when joining and leaving)
    # also can be used as a prefix for the user's name if the option is enabled.
    # Shown as a prefix only when the user has 1 job.
    #
    # NOTE: Must be 1 word
    fullname: Woodcutter
    # Shortened version of the name of the job. Used as a prefix when the user has more 
    # than 1 job
    shortname: WC
    description: Earns money felling and planting trees  
    # The colour of the name, for a full list of supported colours, go to the message config.
    ChatColour: GREEN
    # Option to let you choose what kind of prefix this job adds to your name.
    # options are: full, title, job, shortfull, shorttitle, shortjob and none
    chat-display: full
    # [OPTIONAL] - the maximum level of this class
    #max-level: 10
    # [OPTIONAL] - the maximum level of this class with specific permission
    # use jobs.[jobsname].vipmaxlevel
    #vip-max-level: 20
    # [OPTIONAL] - the maximum number of users on the server that can have this job at 
    # any one time (includes offline players).
    #slots: 1
    # Equation used for calculating how much experience is needed to go to the next level.
    # Available parameters:
    #   numjobs - the number of jobs the player has
    #   joblevel - the level the player has attained in the job.
    # NOTE: Please take care of the brackets when modifying this equation.
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    # Equation used for calculating how much income is given per action for the job level.
    # Available parameters:
    #   baseincome - the income for the action at level 1 (as set in the configuration).
    #   joblevel - the level the player has attained in the job.
    # NOTE: Please take care of the brackets when modifying this equation.
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
     # Equation used for calculating how much experience is given per action for the job level.
    # Available parameters:
    #   baseexperience - the experience for the action at level 1 (as set in the configuration).
    #   joblevel - the level the player has attained in the job.
    # NOTE: Please take care of the brackets when modifying this equation.
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    ########################################################################
    # Section used to configure what items the job gets paid for, how much
    # they get paid and how much experience they gain.
    #
    # For break and place, the block name or id is used.
    # You can select a sub-type by using a '-' between the id and the bit
    # value for the sub-type. e.g LOG-0 = usual log, LOG-2 = birch log
    # 17-2 = birch log.
    #
    # If no sub-type is give, the payout will be for all sub-types.
    #
    # To get a list of all available block types, check the
    # bukkit JavaDocs for a complete list of block types
    # https://hub.spigotmc.org/javadocs/spigot/org/bukkit/Material.html
    # 
    # For kill tags (Kill and custom-kill), the name is the name of the
    # mob.
    # Available mobs:
    #   Bat
    #   Blaze
    #   Cave_spider
    #   Chicken
    #   Cow
    #   Creeper
    #   Ender_dragon
    #   Enderman
    #   Endermite
    #   Ghast
    #   Giant
    #   Guardian
    #   Horse
    #   Iron_golem
    #   Magma_cube
    #   Mushroom_cow
    #   Ocelot
    #   Pig
    #   Player
    #   Rabbit
    #   Sheep
    #   Silverfish
    #   Skeleton
    #   Slime
    #   Snowman
    #   Spider
    #   Squid
    #   Villager
    #   Witch
    #   Wither
    #   Wolf
    #   Zombie   
    #
    # NOTE: mob names are case sensitive.
    #
    # For custom-kill, it is the name of the job (also case sensitive).
    # 
    # NOTE: If a job has both the pay for killing a player and for killing a
    # specific class, they will get both payments.
    ########################################################################
    # payment for breaking a block
    Gui:
      Id: 17
      Data: 2
    Break:
      # block name/id (with optional sub-type)
      LOG:
        # base income
        income: 0.5
        # base experience
        experience: 0.5
    # payment for placing a block
    Place:
      SAPLING: 
        income: 0.05
        experience: 0.5
      WOOD: 
        income: 0.5
        experience: 0.2
      WOOD_STAIRS: 
        income: 0.3
        experience: 0.15
      WOOD_STEP: 
        income: 0.2
        experience: 0.1
      FENCE: 
        income: 0.15
        experience: 0.05

  Miner:
    fullname: Miner
    shortname: Mi
    description: Earns money mining minerals and ores.
    ChatColour: DARK_GRAY
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 56
      Data: 0
    Break:
      STONE:
        income: 0.02
        experience: 0.02
      COAL_ORE:
        income: 0.1
        experience: 0.1
      GLOWING_REDSTONE_ORE:
        income: 0.2
        experience: 0.2
      IRON_ORE: 
        income: 1.0
        experience: 1.0
      GOLD_ORE:
        income: 2.0
        experience: 2.0
      LAPIS_ORE:
        income: 1.0
        experience: 0.1
      DIAMOND_ORE:
        income: 6.0
        experience: 0.5
      OBSIDIAN: 
        income: 7.5
        experience: 0.5
      MOSSY_COBBLESTONE:
        income: 0.03
        experience: 3.0
    Place:
      RAILS:
        income: 1.0
        experience: 2.0
      IRON_ORE:
        income: -1.0
        experience: -1.0
      GOLD_ORE:
        income: -2.0
        experience: -2.0
  Builder:
    fullname: Builder
    shortname: Bd
    description: Earns money for building structures.
    ChatColour: WHITE
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 45
      Data: 0
    Place:
      COBBLESTONE:
        income: 0.01
        experience: 0.01
      WOOD:
        income: 0.5
        experience: 0.5
      FENCE:
        income: 0.5
        experience: 0.5
      WOOL:
        income: 0.5
        experience: 0.5
      STONE:
        income: 0.25
        experience: 0.25
      SMOOTH_BRICK:
        income: 0.25
        experience: 0.25
      GLOWSTONE:
        income: 0.03
        experience: 0.03
      SEA_LANTERN:
        income: 0.03
        experience: 0.03
      SANDSTONE:
        income: 0.01
        experience: 0.0
      GLASS:
        income: 0.10
        experience: 0.01
      BRICK:
        income: 0.04
        experience: 0.01
      LAPIS_BLOCK:
        income: 0.01
        experience: 0.10
      DOUBLE_STEP:
        income: 0.02
        experience: 0.02
      STEP:
        income: 0.01
        experience: 0.01
      BOOKSHELF:
        income: 0.05
        experience: 0.03
      WOOD_STAIRS:
        income: 0.02
        experience: 0.02
      COBBLESTONE_STAIRS:
        income: 0.02
        experience: 0.02
      SMOOTH_STAIRS:
        income: 0.02
        experience: 0.02
      MOSSY_COBBLESTONE:
        income: 0.05
        experience: 1.0
      DIAMOND_BLOCK:
        income: 0.01
        experience: 0.05
      GOLD_BLOCK:
        income: 0.10
        experience: 0.01
      159:
        income: 0.03
        experience: 0.02
  Digger:
    fullname: Digger
    shortname: Dg
    description: Earns money for terraforming the world.
    ChatColour: GOLD
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 2
      Data: 0
    Break:
      DIRT:
        income: 0.05
        experience: 0.05
      GRASS:
        income: 0.02
        experience: 0.2
      GRAVEL:
        income: 0.03
        experience: 0.03
      SAND:
        income: 0.04
        experience: 0.04
      CLAY:
        income: 0.1
        experience: 0.1
  Farmer:
    fullname: Farmer
    shortname: Fa
    description: Earns money farming crops.
    ChatColour: BLUE
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 292
      Data: 0
    Break:
      CROPS-7:
        income: 0.02
        experience: 0.02
      CARROT-7:
        income: 0.02
        experience: 0.02
      POTATO-7:
        income: 0.02
        experience: 0.02
      SUGAR_CANE_BLOCK:
        income: 0.04
        experience: 0.04
      MELON_BLOCK:
        income: 0.05
        experience: 0.05
    Place:
      CROPS-0:
        income: 0.03
        experience: 0.03
      CARROT-0:
        income: 0.03
        experience: 0.03
      POTATO-0:
        income: 0.03
        experience: 0.03
      SUGAR_CANE_BLOCK:
        income: 0.01
        experience: 0.01
  Hunter:
    fullname: Hunter
    shortname: Hu
    description: Earns money killing animals and monsters.
    ChatColour: RED
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 261
      Data: 0
    Kill:
      Chicken:
        income: 0.5
        experience: 0.5
      Cow:
        income: 0.5
        experience: 0.5
      Pig:
        income: 0.5
        experience: 0.5
      Sheep: 
        income: 0.5
        experience: 0.5
      Creeper: 
        income: 1.0
        experience: 1.0
      Skeleton: 
        income: 1.0
        experience: 1.0
      WitherSkeleton:
        income: 2.0
        experience: 2.0
      Spider:
        income: 1.0
        experience: 1.0
      Zombie: 
        income: 1.0
        experience: 1.0
  Fisherman:
    fullname: Fisherman
    shortname: Fi
    description: Earns money from fishing.
    ChatColour: AQUA
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 346
      Data: 0
    Fish:
      RAW_FISH:
        income: 1.5
        experience: 1.5
  Weaponsmith:
    fullname: Weaponsmith
    shortname: Ws
    description: Earns money from crafting and repairing weapons.
    ChatColour: DARK_PURPLE
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 267
      Data: 0
    Craft:
      WOOD_SWORD:
        income: 1.0
        experience: 1.0
      IRON_SWORD:
        income: 2.0
        experience: 2.0
      GOLD_SWORD:
        income: 3.0
        experience: 3.0
      DIAMOND_SWORD:
        income: 4.0
        experience: 4.0
      BOW:
        income: 1.0
        experience: 1.0
    Repair:
      WOOD_SWORD:
        income: 1.0
        experience: 1.0
      IRON_SWORD:
        income: 2.0
        experience: 2.0
      GOLD_SWORD:
        income: 3.0
        experience: 3.0
      DIAMOND_SWORD:
        income: 4.0
        experience: 4.0
    Smelt:
      IRON_INGOT:
        income: 2.0
        experience: 2.0
      GOLD_INGOT:
        income: 2.0
        experience: 2.0
  Brewer:
    fullname: Brewer
    shortname: Br
    description: Earns money brewing potions.
    ChatColour: LIGHT_PURPLE
    chat-display: full
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 379
      Data: 0
    Brew:
      NETHER_STALK:
        income: 1.0
        experience: 1.0
      REDSTONE:
        income: 2.0
        experience: 2.0
      GLOWSTONE_DUST:
        income: 2.0
        experience: 2.0
      SPIDER_EYE:
        income: 2.0
        experience: 2.0
      FERMENTED_SPIDER_EYE:
        income: 2.0
        experience: 2.0
      BLAZE_POWDER:
        income: 2.0
        experience: 2.0
      SUGAR:
        income: 2.0
        experience: 2.0
      SPECKLED_MELON:
        income: 4.0
        experience: 4.0
      MAGMA_CREAM:
        income: 4.0
        experience: 4.0
      GHAST_TEAR:
        income: 4.0
        experience: 4.0
  Enchanter:
    fullname: Enchanter
    shortname: E
    description: Earns money enchanting weapons.
    ChatColour: DARK_BLUE
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 116
      Data: 0
    Enchant:
      WOOD_SWORD:
        income: 1.5
        experience: 3.0
      LEATHER_BOOTS:
        income: 1.0
        experience: 6.0
      LEATHER_CHESTPLATE:
        income: 2.0
        experience: 6.0
      LEATHER_HELMET:
        income: 1.0
        experience: 6.0
      LEATHER_LEGGINGS:
        income: 2.0
        experience: 6.0
      IRON_SWORD:
        income: 3.0
        experience: 6.0
      IRON_BOOTS:
        income: 2.5
        experience: 9.0
      IRON_CHESTPLATE:
        income: 4.5
        experience: 9.0
      IRON_HELMET:
        income: 2.5
        experience: 9.0
      IRON_LEGGINGS:
        income: 4.5
        experience: 9.0
      GOLD_SWORD:
        income: 4.5
        experience: 15.0
      GOLD_BOOTS:
        income: 2.5
        experience: 15.0
      GOLD_CHESTPLATE:
        income: 5.5
        experience: 15.0
      GOLD_HELMET:
        income: 2.5
        experience: 15.0
      GOLD_LEGGINGS:
        income: 5.5
        experience: 15.0
      DIAMOND_SWORD:
        income: 9.0
        experience: 30.0
      DIAMOND_SPADE:
        income: 5.0
        experience: 30.0
      DIAMOND_PICKAXE:
        income: 10.0
        experience: 30.0
      DIAMOND_AXE:
        income: 10.0
        experience: 30.0
      DIAMOND_HELMET:
        income: 6.0
        experience: 30.0
      DIAMOND_CHESTPLATE:
        income: 12.0
        experience: 50.0
      DIAMOND_LEGGINGS:
        income: 12.0
        experience: 50.0
      DIAMOND_BOOTS:
        income: 6.0
        experience: 30.0
      ARROW_DAMAGE-1:
        income: 3.0
        experience: 10.0
      ARROW_DAMAGE-2:
        income: 6.0
        experience: 20.0
      ARROW_DAMAGE-3:
        income: 9.0
        experience: 30.0
      ARROW_DAMAGE-4:
        income: 12.0
        experience: 40.0
      ARROW_DAMAGE-5:
        income: 15.0
        experience: 50.0
      ARROW_FIRE:
        income: 10.0
        experience: 30.0
      ARROW_INFINITE:
        income: 20.0
        experience: 50.0
      ARROW_KNOCKBACK-1:
        income: 3.0
        experience: 10.0
      ARROW_KNOCKBACK-2:
        income: 6.0
        experience: 20.0
      DAMAGE_ALL-1:
        income: 10.0
        experience: 10.0
      DAMAGE_ALL-2:
        income: 20.0
        experience: 20.0
      DAMAGE_ALL-3:
        income: 30.0
        experience: 30.0
      DAMAGE_ALL-4:
        income: 40.0
        experience: 40.0
      DAMAGE_ALL-5:
        income: 50.0
        experience: 50.0
      DAMAGE_ARTHROPODS-1:
        income: 3.0
        experience: 10.0
      DAMAGE_ARTHROPODS-2:
        income: 6.0
        experience: 20.0
      DAMAGE_ARTHROPODS-3:
        income: 9.0
        experience: 30.0
      DAMAGE_ARTHROPODS-4:
        income: 12.0
        experience: 40.0
      DAMAGE_ARTHROPODS-5:
        income: 15.0
        experience: 50.0
      DAMAGE_UNDEAD-1:
        income: 3.0
        experience: 10.0
      DAMAGE_UNDEAD-2:
        income: 6.0
        experience: 20.0
      DAMAGE_UNDEAD-3:
        income: 9.0
        experience: 30.0
      DAMAGE_UNDEAD-4:
        income: 12.0
        experience: 40.0
      DAMAGE_UNDEAD-5:
        income: 15.0
        experience: 50.0
      DEPTH_STRIDER-1:
        income: 3.0
        experience: 10.0
      DEPTH_STRIDER-2:
        income: 6.0
        experience: 20.0
      DEPTH_STRIDER-3:
        income: 9.0
        experience: 30.0
      DIG_SPEED-1:
        income: 3.0
        experience: 10.0
      DIG_SPEED-2:
        income: 6.0
        experience: 20.0
      DIG_SPEED-3:
        income: 9.0
        experience: 30.0
      DIG_SPEED-4:
        income: 12.0
        experience: 40.0
      DIG_SPEED-5:
        income: 15.0
        experience: 50.0
      DURABILITY-1:
        income: 3.0
        experience: 10.0
      DURABILITY-2:
        income: 6.0
        experience: 20.0
      DURABILITY-3:
        income: 9.0
        experience: 30.0
      FIRE_ASPECT-1:
        income: 3.0
        experience: 10.0
      FIRE_ASPECT-2:
        income: 6.0
        experience: 20.0
      KNOCKBACK-1:
        income: 3.0
        experience: 10.0
      KNOCKBACK-2:
        income: 6.0
        experience: 20.0
      LOOT_BONUS_BLOCKS-1:
        income: 20.0
        experience: 100.0
      LOOT_BONUS_BLOCKS-2:
        income: 40.0
        experience: 200.0
      LOOT_BONUS_BLOCKS-3:
        income: 80.0
        experience: 300.0
      LOOT_BONUS_MOBS-1:
        income: 5.0
        experience: 20.0
      LOOT_BONUS_MOBS-2:
        income: 12.0
        experience: 40.0
      LOOT_BONUS_MOBS-3:
        income: 15.0
        experience: 60.0
      LUCK-1:
        income: 15.0
        experience: 10.0
      LUCK-2:
        income: 25.0
        experience: 20.0
      LUCK-3:
        income: 35.0
        experience: 30.0
      LURE-1:
        income: 10.0
        experience: 10.0
      LURE-2:
        income: 20.0
        experience: 20.0
      LURE-3:
        income: 30.0
        experience: 30.0
      OXYGEN-1:
        income: 3.0
        experience: 10.0
      OXYGEN-2:
        income: 6.0
        experience: 20.0
      OXYGEN-3:
        income: 9.0
        experience: 30.0
      PROTECTION_ENVIRONMENTAL-1:
        income: 5.0
        experience: 10.0
      PROTECTION_ENVIRONMENTAL-2:
        income: 10.0
        experience: 20.0
      PROTECTION_ENVIRONMENTAL-3:
        income: 15.0
        experience: 30.0
      PROTECTION_ENVIRONMENTAL-4:
        income: 20.0
        experience: 40.0
      PROTECTION_EXPLOSIONS-1:
        income: 5.0
        experience: 10.0
      PROTECTION_EXPLOSIONS-2:
        income: 10.0
        experience: 20.0
      PROTECTION_EXPLOSIONS-3:
        income: 15.0
        experience: 30.0
      PROTECTION_EXPLOSIONS-4:
        income: 20.0
        experience: 40.0
      PROTECTION_FALL-1:
        income: 3.0
        experience: 10.0
      PROTECTION_FALL-2:
        income: 6.0
        experience: 20.0
      PROTECTION_FALL-3:
        income: 9.0
        experience: 30.0
      PROTECTION_FALL-4:
        income: 12.0
        experience: 40.0
      PROTECTION_FIRE-1:
        income: 5.0
        experience: 10.0
      PROTECTION_FIRE-2:
        income: 10.0
        experience: 20.0
      PROTECTION_FIRE-3:
        income: 15.0
        experience: 30.0
      PROTECTION_FIRE-4:
        income: 20.0
        experience: 40.0
      PROTECTION_PROJECTILE-1:
        income: 10.0
        experience: 10.0
      PROTECTION_PROJECTILE-2:
        income: 20.0
        experience: 20.0
      PROTECTION_PROJECTILE-3:
        income: 30.0
        experience: 30.0
      PROTECTION_PROJECTILE-4:
        income: 40.0
        experience: 40.0
      SILK_TOUCH:
        income: 100.0
        experience: 300.0
      THORNS-1:
        income: 4.0
        experience: 10.0
      THORNS-2:
        income: 8.0
        experience: 20.0
      THORNS-3:
        income: 12.0
        experience: 30.0
      WATER_WORKER:
        income: 30.0
        experience: 100.0
  Engineer:
    fullname: Engineer
    shortname: En
    description: Earns money with redstone
    ChatColour: RED
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 356
      Data: 0
    Place:
      REDSTONE_WIRE:
        income: 0.01
        experience: 0.01
      DIODE_BLOCK_OFF:
        income: 0.3
        experience: 0.15
      DIODE_BLOCK_ON:
        income: 0.3
        experience: 0.15
      REDSTONE_COMPARATOR_OFF:
        income: 0.7
        experience: 0.25
      REDSTONE_COMPARATOR_ON:
        income: 0.7
        experience: 0.25
      REDSTONE_LAMP_ON:
        income: 0.3
        experience: 0.15
      REDSTONE_LAMP_OFF:
        income: 0.3
        experience: 0.15
      REDSTONE_BLOCK:
        income: 0.2
        experience: 0.12
      REDSTONE_TORCH_ON:
        income: 0.2
        experience: 0.12
      REDSTONE_TORCH_OFF:
        income: 0.2
        experience: 0.12
      PISTON_BASE:
        income: 0.5
        experience: 0.23
      PISTON_STICKY_BASE:
        income: 0.6
        experience: 0.28
      ACTIVATOR_RAIL:
        income: 0.4
        experience: 0.21
  Rancher:
    fullname: Rancher
    shortname: Ra
    description: Earns money managing animals.
    ChatColour: BLUE
    chat-display: full
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Gui:
      Id: 420
      Data: 0
    Tame:
      Wolf:
        income: 0.5
        experience: 0.2
      Horse:
        income: 0.5
        experience: 0.2
    Breed:
      Mushroomcow:
        income: 0.25
        experience: 0.1
      Cow:
        income: 0.25
        experience: 0.1
      Chicken:
        income: 0.25
        experience: 0.1
      Pig:
        income: 0.25
        experience: 0.1
      Sheep:
        income: 0.25
        experience: 0.1
      Horse:
        income: 1.25
        experience: 0.3
    Milk:
      Cow:
        income: 0.2
        experience: 0.1
      Mushroomcow:
        income: 0.2
        experience: 0.1
    Kill:
      Chicken:
        income: 0.5
        experience: 0.5
      Cow:
        income: 0.5
        experience: 0.5
      Pig:
        income: 0.5
        experience: 0.5
      Sheep: 
        income: 0.5
        experience: 0.5
    Shear:
      White:
        income: 0.05
        experience: 0.02
      Blue:
        income: 0.05
        experience: 0.02
      Black:
        income: 0.05
        experience: 0.02
      Light_blue:
        income: 0.05
        experience: 0.02
      Yellow:
        income: 0.05
        experience: 0.02
      Green:
        income: 0.05
        experience: 0.02
      Silver:
        income: 0.05
        experience: 0.02
      Red:
        income: 0.05
        experience: 0.02
      Purple:
        income: 0.05
        experience: 0.02
      Pink:
        income: 0.05
        experience: 0.02
      Orange:
        income: 0.05
        experience: 0.02
      Magenta:
        income: 0.05
        experience: 0.02
      Lime:
        income: 0.05
        experience: 0.02
      Cyan:
        income: 0.05
        experience: 0.02
      Brown:
        income: 0.05
        experience: 0.02
  None:
    fullname: None
    shortname: N
    ChatColour: WHITE
    chat-display: none
    #max-level: 10
    #slots: 10
    leveling-progression-equation: 100*((1.13+(0.01*(numjobs-1)))^(joblevel-1))
    income-progression-equation: baseincome*((1.05)^(joblevel-1))
    experience-progression-equation: baseexperience*((1.05)^(joblevel-1))
    Kill:
      Player:
        income: 0.5